import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Redirect, Switch} from 'react-router-dom'
import './index.css';
// import App from './App';
import Login from './components/login/login';
import * as serviceWorker from './serviceWorker';
import MainPage from './components/mainPage/mainPage';
import NewMainPage from './components/newMainPage/newMainPage'
import Profile from './components/profile/profile';
import Home from './components/home/home';
import Card from './components/card/card';
import Cart from './components/cart/cart';

const CoreApp = () => (
    <BrowserRouter>
        <Switch>
            <Route path="/login" component={Login}/> 
            <Route path="/mainpage" component={MainPage}/>
            <Route path="/newmainpage" component={NewMainPage} />
            <Route path="/profile" component={Profile}/>
            <Route path="/home" component={Home} />
            <Route path="/card" component={Card} />
            <Route path="/cart" component={Cart} />
            <Redirect from="/" to="/login" />
        </Switch>
    </BrowserRouter>
);



ReactDOM.render(<CoreApp />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
