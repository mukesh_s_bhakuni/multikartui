import {DOMAIN} from '../../constants/http';


export function GetUserDetailsFromToken(tokenId) {
    return fetch(DOMAIN + `multiKart/user/?tokenId=${tokenId}`, {
        method: 'GET',
    })
        .then(res => res.json())
        .then((result) => {
            return result
        },
            (error) => {
                console.log(error)
                return error
            }
        ).catch((error) => { console.log(error) });
}
