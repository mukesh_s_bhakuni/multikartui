import React from 'react';
import { Tabs, Tab, TabList, TabPanel } from 'react-tabs';
import "react-tabs/style/react-tabs.css";
import Profile from '../profile/profile';
import { GetUserDetailsFromToken } from './apiMethods.js';
import { errorToast } from '../utils/utils';
import { ToastContainer } from 'react-toastify';
import Home from '../home/home';

class NewMainPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        var token = localStorage.getItem("__Xtoken__");
        GetUserDetailsFromToken(token).then((result) => {
            if (result.status) {
                this.setState({
                    userDetails: result.responseData,
                });
            } else {
                this.props.history.push("login");
                errorToast("Session Expired, Please Login Again");
            }
        });
    }


    render() {
        var userDetails = this.state.userDetails;
        return <div>
            <div>
                <Tabs>
                    <TabList>
                        <Tab>Home</Tab>
                        <Tab>Profile</Tab>
                    </TabList>
                    <TabPanel>
                        <Home userDetails={userDetails} history={this.props.history}/>
                    </TabPanel>
                    <TabPanel>
                        <Profile userDetails={userDetails} />
                    </TabPanel>
                </Tabs>
            </div>
            <ToastContainer autoClose={4000} />
        </div>
    }
}

export default NewMainPage;