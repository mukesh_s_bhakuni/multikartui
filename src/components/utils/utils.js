import { toast } from 'react-toastify';
import React from 'react';
import './dropDown.css';

export function successToast(message)  {
    toast.dismiss();
    toast.dismiss();
    toast.success(message, {
        position: toast.POSITION.TOP_CENTER,
        autoClose: 5000
    });
}



export function errorToast(err){
    toast.dismiss();
    toast.dismiss();
    toast.error(err + "!!!", {
        position: toast.POSITION.TOP_CENTER,
        autoClose: 7000
    });
}


export function DropDown(props = { name: "", value: [], default: "",style : {marginLeft  : "10px",marginTop : "5px"} }) {
    const mappingFunction = p => <option key={p} value={p}>{p}</option>;
    return (
        <div>
            <select
                style={props.style}
                className="custom-select"
                value={props.default}
                name={props.name}
                type="text"
                onChange={props.onChange}
                onBlur={props.onBlur}
                required
            >
                {(props.value !== null) && props.value.map(mappingFunction)}
            </select>
        </div>
    );
}