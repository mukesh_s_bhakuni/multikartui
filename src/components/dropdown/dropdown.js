import React from "react";

export function DropDown(props = { name: "", value: [], default: "" }) {
    const mappingFunction = p => <option key={p} value={p}>{p}</option>;
    return (
        <div>
            <select
                className="custom-select"
                style={{ width: "240px" , height:"38px", fontSize:"12px" }}
                value={props.default}
                name={props.name}
                type="text"
                onChange={props.onChange}
                onBlur={props.onBlur}
                required
            >
                {props.value.map(mappingFunction)}
            </select>
        </div>
    );
}