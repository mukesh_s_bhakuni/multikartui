import React from 'react';
import './profile.css';
import { Input } from 'semantic-ui-react';
import { successToast, errorToast } from '../utils/utils';
import { SaveUserData } from './apiMethods';
import { Label } from 'reactstrap';


class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userid: "",
            addressFlag: false,
            detailsFlag: false,
            cancelDetailsFlag: false,
            cancelAddressFlag: false,
            firstName: "",
            middleName: "",
            lastname: "",
            username: "",
            password: "",
            emailId: "",
            mobile: "",
            flatNo: "",
            society: "",
            street: "",
            pincode: "",
            country: "",
            state: "",
            city: "",
            landmark: "",
            emailmsg: "",
            mobilemsg: "",
            pincodemsg: "",
        }
    }

    componentDidMount() {
        this.userDetails();
    }


    handleChange = (event) => {
        var name = event.target.name
        this.setState({ [name]: event.target.value }, () => {
            switch (name) {
                case "emailId":
                    if (!this.validateEmail()) {
                        this.setState({ emailmsg: "invalid email" });
                    } else {
                        this.setState({ emailmsg: "" });
                    }
                    break;
                case "mobile":
                    if (!this.validateMobile()) {
                        this.setState({ mobilemsg: "invalid mobile" });
                    } else {
                        this.setState({ mobilemsg: "" });
                    }
                    break;
                case "pincode":
                    if (!this.validatePincode()) {
                        this.setState({ pincodemsg: "invalid pincode" });
                    } else {
                        this.setState({ pincodemsg: "" });
                    }
                    break;
                default:
                    console.log("invalid case");
            }
        });
    }



    userDetails = () => {
        var userDetails = this.props.userDetails;
        var userAddress = this.props.userDetails.user_address;
        if (userDetails === undefined) {
            return;
        }
        this.setState({
            userid: userDetails._id,
            firstName: userDetails.userFirstName,
            middleName: userDetails.userMiddleName,
            lastname: userDetails.userLastName,
            mobile: userDetails.mobile[0],
            emailId: userDetails.emailId,
            username: userDetails.userName,
            flatNo: userAddress[0].flat_no,
            society: userAddress[0].society,
            landmark: userAddress[0].landmark,
            city: userAddress[0].city,
            state: userAddress[0].state,
            street: userAddress[0].street,
            pincode: userAddress[0].pin_code,
            country: userAddress[0].country,
            password: userDetails.password
        });
    }

    editAddress = () => {
        this.setState({
            addressFlag: true,
            cancelAddressFlag: true,
        });
    }



    saveAddress = () => {
        this.saveUserData({ addressFlag: false, cancelAddressFlag: false });
    }

    editDetails = () => {
        this.setState({
            detailsFlag: true,
            cancelDetailsFlag: true,
        });
    }

    saveDetails = () => {
        this.saveUserData({ detailsFlag: false, cancelDetailsFlag: false });
    }


    validateEmail = () => {
        var email = this.state.emailId;
        var regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (regex.test(email)) {
            return true;
        }
        return false;
    }

    validateMobile = () => {
        var mobile = this.state.mobile;
        var regex = /^[6-9]\d{9}$/
        if (regex.test(mobile)) {
            return true;
        }
        return false;
    }

    validatePincode = () => {
        var pincode = this.state.pincode;
        var regex = /^[1-9][0-9]{5}$/
        if (regex.test(pincode)) {
            return true;
        }
        return false;
    }

    validateAll = () => {
        let msg = "";
        var username = (this.state.username === null || this.state.username === "" || this.state.username === undefined);
        var firstname = (this.state.firstName === null || this.state.firstName === "" || this.state.firstName === undefined);
        var lastname = (this.state.lastname === null || this.state.lastname === "" || this.state.lastname === undefined);
        var flatno = (this.state.flatNo === null || this.state.flatNo === "" || this.state.flatNo === undefined);
        var society = (this.state.society === null || this.state.society === "" || this.state.society === undefined);
        var street = (this.state.street === null || this.state.street === "" || this.state.street === undefined);
        var landmark = (this.state.landmark === null || this.state.landmark === "" || this.state.landmark === undefined);
        var city = (this.state.city === null || this.state.city === "" || this.state.city === undefined);
        var state = (this.state.state === null || this.state.state === "" || this.state.state === undefined);
        var country = (this.state.country === null || this.state.country === "" || this.state.country === undefined);
        if (this.validateEmail() && this.validateMobile() && this.validatePincode()) {
            if (firstname || lastname) {
                msg += "[name]"
            }
            if (username) {
                msg += "[username]"
            }
            if (flatno || society || street || landmark || city || state || country) {
                msg += "[all address fields] "
            }
            if (msg === "") {
                return msg
            } else {
                msg += " required in proper format"
                return msg;
            }
        } else {
            msg = "all credentials required in proper format"
            return msg;
        }
    }


    saveUserData = (options) => {
        var msg = this.validateAll();
        if (msg !== "") {
            errorToast(msg);
            return;
        }
        var body = JSON.stringify({
            _id: this.state.userid,
            userFirstName: this.state.firstName,
            userMiddleName: this.state.middleName,
            userLastName: this.state.lastname,
            emailId: this.state.emailId,
            userName: this.state.username,
            password: this.state.password,
            mobile: [this.state.mobile],
            user_address: [{
                flat_no: this.state.flatNo,
                society: this.state.society,
                street: this.state.street,
                landmark: this.state.landmark,
                city: this.state.city,
                state: this.state.state,
                pin_code: this.state.pincode,
                country: this.state.country
            }]
        });
        SaveUserData(body).then((result) => {
            if (result.status) {
                this.setState(options, () => {
                    successToast("Data Saved Successfully");
                })
            } else {
                errorToast("Unable To Save Data");
            }
        });
    }



    cancelAddress = () => {
        this.setState({
            addressFlag: false,
            cancelAddressFlag: false,
        });
    }

    cancelDetails = () => {
        this.setState({
            detailsFlag: false,
            cancelDetailsFlag: false,
        });
    }

    render() {
        return <div>
            <div className="profile-dialog">
                <div className="profile-dialog-outerbox">
                    <div style={{ textAlign: "center" }}>
                        <b>Profile Details</b>
                    </div>
                    <br /><br />
                    {this.state.detailsFlag && <div>
                        <div className="row">
                            <div className="col-sm-2"
                                style={{
                                    display: "inline-block",
                                    marginLeft: "10px"
                                }}
                            >
                                <Label><u>First Name</u></Label>
                                <Input
                                    name="firstName"
                                    value={this.state.firstName}
                                    placeholder="First Name"
                                    onChange={this.handleChange}
                                />
                                
                            </div>
                            <div className="col-sm-2"
                                style={{
                                    display: "inline-block",
                                    marginLeft: "35px"
                                }}
                            >
                                <Label><u>Middle Name</u></Label>
                                <Input
                                    name="middleName"
                                    value={this.state.middleName}
                                    placeholder="Middle Name"
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="col-sm-2"
                                style={{
                                    display: "inline-block",
                                    marginLeft: "35px"
                                }}
                            >
                                <Label><u>Last Name</u></Label>
                                <Input
                                    name="lastname"
                                    value={this.state.lastname}
                                    placeholder="Last Name"
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <br />
                        <div className="row">
                            <div className="col-sm-2"
                                style={{
                                    display: "inline-block",
                                    marginLeft: "15px"
                                }}
                            >
                                <Label><u>Username</u></Label>
                                <Input
                                    name="username"
                                    value={this.state.username}
                                    placeholder="Username"
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="col-sm-2"
                                style={{
                                    display: "inline-block",
                                    marginLeft: "35px"
                                }}
                            >
                                <Label><u>Email Id</u></Label>
                                <Input
                                    name="emailId"
                                    value={this.state.emailId}
                                    placeholder="Email Id"
                                    onChange={this.handleChange}
                                />
                                <Label
                                style={{
                                    color : "red"
                                }}
                                >{this.state.emailmsg}</Label>
                            </div>
                            <div className="col-sm-2"
                                style={{
                                    display: "inline-block",
                                    marginLeft: "35px"
                                }}
                            >
                                <Label><u>Mobile</u></Label>
                                <Input
                                    name="mobile"
                                    value={this.state.mobile}
                                    placeholder="Mobile"
                                    onChange={this.handleChange}
                                />
                                <Label
                                style={{
                                    color : "red"
                                }}
                                >{this.state.mobilemsg}</Label>
                            </div>
                        </div>
                    </div>}
                    {!this.state.detailsFlag && !this.state.cancelDetailsFlag &&
                        <div
                            style={{
                                marginLeft: "25px",
                                textAlign: "left"
                            }}
                        >

                            <b>Name : {this.state.firstName} - {this.state.middleName} - {this.state.lastname}</b>
                            <br />
                            <b>Username : {this.state.username}</b>
                            <br />
                            <b>Email : {this.state.emailId}</b>
                            <br />
                            <b>Mobile : {this.state.mobile}</b>

                        </div>
                    }
                    <br />
                    {this.state.detailsFlag && this.state.cancelDetailsFlag &&
                        <button
                            className="other"
                            onClick={this.saveDetails}
                        >
                            save
                    </button>
                    }
                    {!this.state.cancelDetailsFlag && !this.state.detailsFlag &&
                        <button
                            className="other"
                            onClick={this.editDetails}
                        >
                            edit
                    </button>
                    }

                    {this.state.detailsFlag && this.state.cancelDetailsFlag &&
                        <button
                            className="failure"
                            onClick={this.cancelDetails}
                        >
                            cancel
                    </button>
                    }

                    <br /><br />
                    <div style={{ marginLeft: "10px", textAlign: "left", color: "smokewhite" }}>
                        <b><u>Address</u></b>
                    </div>
                    <br />
                    {this.state.addressFlag && <div>
                        <div className="row"
                         style = {{
                             display : "flex",
                             justifyContent : "space-between"
                         }}
                        >
                            <div className="col-sm-2"
                                style={{
                                    display: "inline-block",
                                    marginLeft: "10px"
                                }}
                            >
                                <Label><u>Flat No</u></Label>
                                <Input
                                    name="flatNo"
                                    value={this.state.flatNo}
                                    placeholder="Flat No"
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="col-sm-2"
                                style={{
                                    display: "inline-block",
                                    marginLeft: "35px"
                                }}
                            >
                                <Label><u>Society</u></Label>
                                <Input
                                    name="society"
                                    value={this.state.society}
                                    placeholder="Society"
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="col-sm-2"
                                style={{
                                    display: "inline-block",
                                    marginLeft: "35px"
                                }}
                            >
                                <Label><u>Street</u></Label>
                                <Input
                                    name="street"
                                    value={this.state.street}
                                    placeholder="Street"
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="col-sm-2"
                                style={{
                                    display: "inline-block",
                                    marginLeft: "35px"
                                }}
                            >
                                <Label><u>Pincode</u></Label>
                                <Input
                                    name="pincode"
                                    value={this.state.pincode}
                                    placeholder="Pincode"
                                    onChange={this.handleChange}
                                />
                                <Label
                                style={{
                                    color : "red"
                                }}
                                >{this.state.pincodemsg}</Label>
                            </div>
                        </div>
                        <div className="row"
                         style={{
                            display : "flex",
                            justifyContent : "space-between"                            
                         }}
                        >
                            <div className="col-sm-2"
                                style={{
                                    display: "inline-block",
                                    marginLeft: "10px"
                                }}
                            >
                                <Label><u>Landmark</u></Label>
                                <Input
                                    name="landmark"
                                    value={this.state.city}
                                    placeholder="Landmark"
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="col-sm-2"
                                style={{
                                    display: "inline-block",
                                    marginLeft: "35px"
                                }}
                            >
                                <Label><u>City</u></Label>
                                <Input
                                    name="city"
                                    value={this.state.city}
                                    placeholder="City"
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="col-sm-2"
                                style={{
                                    display: "inline-block",
                                    marginLeft: "35px"
                                }}
                            >   
                                <Label><u>State</u></Label>
                                <Input
                                    name="state"
                                    value={this.state.state}
                                    placeholder="State"
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="col-sm-2"
                                style={{
                                    display: "inline-block",
                                    marginLeft: "35px"
                                }}
                            >   
                                <Label><u>Country</u></Label>
                                <Input
                                    name="country"
                                    value={this.state.country}
                                    placeholder="Country"
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                    </div>}
                    {
                        !this.state.addressFlag && !this.state.cancelAddressFlag &&
                        <div
                            style={{
                                marginLeft: "25px"
                            }}
                        >

                            <b>Flat No : {this.state.flatNo} </b>
                            <br />
                            <b>Society : {this.state.society} </b>
                            <br />
                            <b>Landmark : {this.state.landmark}</b>
                            <br />
                            <b>City : {this.state.city}</b>
                            <br />
                            <b>Street : {this.state.street}</b>
                            <br />
                            <b>Pincode : {this.state.pincode}</b>
                            <br />
                            <b>State : {this.state.state}</b>
                            <br />
                            <b>Country : {this.state.country}</b>
                        </div>
                    }
                    <br />
                    {this.state.addressFlag && this.state.cancelAddressFlag &&
                        <button
                            className="other"
                            onClick={this.saveAddress}
                        >
                            save
                    </button>
                    }
                    {!this.cancelAddressFlag && !this.state.addressFlag &&
                        <button
                            onClick={this.editAddress}
                            className="other"
                        >
                            edit
                    </button>
                    }

                    {this.state.addressFlag && this.state.cancelAddressFlag &&
                        <button
                            className="failure"
                            onClick={this.cancelAddress}
                        >
                            cancel
                        </button>
                    }
                </div>
            </div>
        </div>
    }
}


export default Profile;