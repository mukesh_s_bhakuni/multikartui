

import { DOMAIN } from '../../constants/http';


export function SaveUserData(jsonBody) {
    return fetch(DOMAIN + `multiKart/user/?type=updateBody`, {
        method: 'PUT',
        body: jsonBody,
    })
        .then(res => res.json())
        .then((result) => {
            return result
        },
            (error) => {
                console.log(error)
                return error
            }
        ).catch((error) => { console.log(error) });
}