import React from 'react';
import './home.css';
import { Label, Input } from 'reactstrap';
import Icon from 'react-icons-kit';
import { androidSettings } from 'react-icons-kit/ionicons/androidSettings'
import { logOut, shoppingCart } from 'react-icons-kit/feather/'
import { search } from 'react-icons-kit/fa/search'
import { firstOrder } from 'react-icons-kit/fa/firstOrder'
import { save } from 'react-icons-kit/fa/save'
import { RemoveUserSession } from './apiMethods';
import { successToast, errorToast, DropDown } from '../utils/utils';
import Card from '../card/card';
import { GetProductList } from '../mainPage/apiMethods';
import { GetAllConstants } from './apiMethods';
import Cart from '../cart/cart';




class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchString: "",
            searchSettingFlag: false,
            category: "All",
            priceRangeSelected: "",
            productList: [],
            priceRange: [{
                priceValue: 1,
                priceString: "Rs 100 to Rs 500",
                isChecked: false,
            }],
            discount: [{
                discountValue: 10,
                discountString: "10%  and above",
                isChecked: false,
            }],
            options: [],
            cart: {},
            prices: [],
            discounts: [],
            pricesString: "",
            discountString: "",
            cartFlag: false,
            itemList: [],
        }
    }


    componentDidMount() {
        GetAllConstants().then((result) => {
            console.log(result);
            this.setState({
                priceRange: result.responseData[0],
                discount: result.responseData[1],
                options: result.responseData[2],
                category: result.responseData[2][0],
            });
        });
    }


    handleDiscountCheck = (value) => {
        let discount = this.state.discount;
        let discounts = this.state.discounts;
        discount.forEach((item) => {
            if (item.discountValue === value) {
                item.isChecked = (!item.isChecked);
                if (item.isChecked) {
                    discounts.push(value);
                    this.setState({
                        discounts: discounts,
                        discountString: discounts.join(",")
                    });
                } else {
                    discounts = discounts.filter(elm => elm !== value);
                    this.setState({
                        discounts: discounts,
                        discountString: discounts.join(","),
                    });
                }
            }
        });
        this.setState({ discount: discount });
    }

    handlePriceCheck = (value) => {
        let priceRange = this.state.priceRange;
        let prices = this.state.prices;
        priceRange.forEach((item) => {
            if (item.priceValue === value) {
                item.isChecked = (!item.isChecked);
                if (item.isChecked) {
                    prices.push(value);
                    this.setState({
                        prices: prices,
                        pricesString: prices.join(","),
                    });
                } else {
                    prices = prices.filter(elm => elm !== value);
                    this.setState({
                        prices: prices,
                        pricesString: prices.join(","),
                    });
                }
            }
        });
        this.setState({ priceRange: priceRange });
    }

    performSearch = () => {
        GetProductList(this.state.searchString, this.state.discountString, this.state.pricesString, this.state.category).then((result) => {
            if (result.status) {
                this.setState({
                    productList: result.responseData,
                    cartFlag : false,
                    searchSettingFlag : false,
                });
            } else {
                errorToast("Unable To Search, Server Error");
            }
        });
    }

    handleChange = (event) => {
        var name = event.target.name;
        this.setState({ [name]: event.target.value });
    }

    priceMappingFunction = (object) => {
        return (
            <div>
                <Label
                    style={{
                        color: "white",
                        marginLeft: "10px",
                        marginTop: "5px",
                    }}
                >{object.priceString}</Label>
                <input
                    type="checkbox"
                    defaultChecked={object.isChecked}
                    onChange={this.handlePriceCheck.bind(this, object.priceValue)}
                />
            </div>
        );
    }

    discountMappingFunction = (object) => {
        return (
            <div>
                <Label
                    style={{
                        color: "white",
                        marginLeft: "10px",
                        marginTop: "5px",
                    }}
                >{object.discountString}</Label>
                <input
                    type="checkbox"
                    defaultChecked={object.isChecked}
                    onChange={this.handleDiscountCheck.bind(this, object.discountValue)}
                />
            </div>
        );
    }

    saveFilters = () => {
        this.setState({
            searchSettingFlag: false
        });
    }

    searchSetting = () => {
        this.setState({
            searchSettingFlag: true,
            cartFlag: false,
            productList : []
        });
    }


    logOut = () => {
        var token = localStorage.getItem("__Xtoken__");
        RemoveUserSession(token).then((result) => {
            if (result.status) {
                this.props.history.push("login");
                localStorage.removeItem("__Xtoken__");
                successToast(result.message);
            } else {
                errorToast(result.message);
            }
        });
    }


    callBackFunction = (option) => {
        if (option === "cancel") {
            this.setState({
                cartFlag: false
            });
        } 
    }


    handleCart = () => {
        this.setState({
            productList : [],
            cartFlag: true,
            searchSettingFlag: false
        });
    }

    makeCartList = () => {
        var itemList = []
        var cart = this.state.cart;
        var keys = Object.keys(cart);
        for (var i = 0; i < keys.length; i++) {
            itemList.push([keys[i], cart[keys[i]]["price"], cart[keys[i]]["quantity"]]);
        }
        this.setState({
            itemList: itemList,
            cartFlag: true,
            searchSettingFlag: false,
        });
    }

    



    render() {

        var priceRange = this.state.priceRange.map(this.priceMappingFunction);
        var discount = this.state.discount.map(this.discountMappingFunction);

        return <div>
            <div>
                <div
                    style={{
                        display: "flex",
                        justifyContent: "space-between",
                    }}
                >
                    <div className="menu-bar"
                        style={{
                            width: "100%",
                            display: "flex",
                            justifyContent: "flex-start",
                            height: "50px"
                        }}
                    >
                        <div
                            style={{
                                display: "flex",
                                justifyContent: "space-between",
                            }}
                        >
                            <div>
                                <button
                                    style={{
                                        background: "white",
                                        width: "40%",
                                        marginTop: "7px"
                                    }}
                                    onClick={this.performSearch}
                                >
                                    <Icon icon={search} />
                                </button>
                            </div>
                            <div>
                                <Input
                                    style={{
                                        color: "black",
                                        background: "white",
                                        width: "500px",
                                        height: "30px"
                                    }}
                                    className="input-box"
                                    name="searchString"
                                    value={this.state.searchString}
                                    placeholder="enter the name of product"
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="menu-bar"
                        style={{
                            display: "flex",
                            justifyContent: "center",
                            width: "100%",
                            height: "50px"
                        }}
                    >
                        <div>
                            <button
                                onClick={this.searchSetting}
                                disabled={this.state.searchSettingFlag}
                                style={{ marginTop: "10px" }}
                            >
                                <Icon icon={androidSettings} />
                            </button>
                        </div>
                        <div>
                            <button
                                disabled={this.state.cartFlag}
                                style={{ marginTop: "10px" }}
                                onClick={this.handleCart}
                            >
                                <Icon icon={shoppingCart} />
                            </button>
                        </div>
                        <div>
                            <button
                                style={{ marginTop: "10px" }}
                            >
                                <Icon icon={firstOrder} />
                            </button>
                        </div>
                        <div>
                            <button
                                style={{ marginTop: "10px" }}
                                onClick={this.logOut}
                            >
                                <Icon icon={logOut} />
                            </button>
                        </div>
                    </div>
                </div>
                <br /><br />
                <div className="scrollable">
                    <Card
                        productList={this.state.productList}
                        callBackFunc={this.callBackFunction}
                    />
                </div>
                {this.state.searchSettingFlag &&
                    <div className="search-setting">
                        <div
                            style={{
                                display: "flex"
                            }}
                        >
                            <Label
                                style={{
                                    marginLeft: "10px",
                                    marginTop: "5px",
                                    color: "white",
                                    fontWeight: "bold"
                                }}
                            >Options </Label>
                            <DropDown
                                name="category"
                                value={this.state.options}
                                default={this.state.category}
                                style={{
                                    marginLeft: "10px",
                                    marginTop: "5px",
                                    height: "20px"
                                }}
                                onChange={this.handleChange}
                            />
                        </div>
                        <br />
                        <div>
                            <Label
                                style={{
                                    color: "white",
                                    fontWeight: "bold",
                                    marginLeft: "10px",
                                    marginTop: "5px",
                                }}
                            >Price</Label>
                            <br />
                            {priceRange}
                        </div>
                        <br />
                        <div>
                            <Label
                                style={{
                                    color: "white",
                                    fontWeight: "bold",
                                    marginLeft: "10px",
                                    marginTop: "5px",
                                }}
                            >Discount</Label>
                            <br />
                            {discount}
                        </div>
                        <button
                            style={{
                                marginTop: "30px",
                                marginLeft: "5px"
                            }}
                            onClick={this.saveFilters}
                        >
                            <Icon icon={save} />
                        </button>
                    </div>
                }
                {
                    this.state.cartFlag && <div
                        style={{
                            overflowY: "scroll"
                        }}
                    >
                        <Cart
                            callBackFunc={this.callBackFunction}
                        />
                    </div>
                }
            </div>
        </div>
    }
}

export default Home;