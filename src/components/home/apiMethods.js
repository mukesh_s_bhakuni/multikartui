import { DOMAIN } from '../../constants/http.js'

export function RemoveUserSession(tokenId) {
    return fetch(DOMAIN + `multiKart/login/?changeType=logout&tokenId=${tokenId}`, {
        method: 'PUT',
    })
        .then(res => res.json())
        .then((result) => {
            return result
        },
            (error) => {
                console.log(error)
                return error
            }
        ).catch((error) => { console.log(error) });
}



export function GetAllConstants() {
    return fetch(DOMAIN + `multiKart/catalogue/?options=constants`,{
        method: 'GET',
    })
        .then(res => res.json())
        .then((result) => {
            return result
        },
            (error) => {
                console.log(error)
                return error
            }
        ).catch((error) => { console.log(error) });
}