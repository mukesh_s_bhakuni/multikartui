
import { DOMAIN } from '../../constants/http.js'

export function GetProductList(searchString,discountValues,priceValues,category) {
    console.log(searchString,discountValues,priceValues,category)
    return fetch(DOMAIN + `multiKart/catalogue/?options=searchString&searchString=${searchString}&discountValues=${discountValues}&priceValues=${priceValues}&category=${category}`, {
        method: 'GET',
    })
        .then(res => res.json())
        .then((result) => {
            return result
        },
            (error) => {
                console.log(error)
                return error
            }
        ).catch((error) => { console.log(error) });
}


export function GetUserDetails(tokenId) {
    return fetch(DOMAIN + `multiKart/user/?tokenId=${tokenId}`, {
        method: 'GET',
    })
        .then(res => res.json())
        .then((result) => {
            return result
        },
            (error) => {
                console.log(error)
                return error
            }
        ).catch((error) => { console.log(error) });
}