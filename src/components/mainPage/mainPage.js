import React from 'react';
import './mainPage.css';
import 'react-toastify/dist/ReactToastify.css';
// import { timingSafeEqual } from 'crypto';
// import { ToastContainer } from 'react-toastify';
import { GetProductList, GetUserDetails } from './apiMethods';
import { errorToast, successToast } from '../utils/utils';
import { ToastContainer } from 'react-toastify';
import "./mainPage.css";



export class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }







    mappingfunction = (item) => {
        return (
            <div>
                Item Name : {item[0]}
                <br />
                Item Price : {item[1]}
                <br />
                Item Quantity : {item[2]}
                <br /><br /><br />
            </div>
        );
    }


    handleOrder = () => {
        successToast("not yet implemented");
    }


    render() {
        var items = this.props.cart.map(this.mappingfunction);
        return (
            <div className="cart-dialog">
                <div className="outer-cart-dialog">
                    <div style={{ textAlign: "center" }}>
                        <b>Cart Items</b>
                        {items}
                    </div>
                    <div>
                        <button
                            className="blue"
                            onClick={this.handleOrder}
                        >
                            Place Order
                        </button>
                        <button
                            className="blue"
                            onClick={this.props.clearCart}
                        >
                            Clear
                        </button>
                        <button
                            className="blue"
                            onClick={this.props.cancel}
                        >
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}


export class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <div className="mydialog">
                <div className="mydialog-outerbox">
                    <div style={{ textAlign: "center" }}>
                        <b>Profile Details</b>
                    </div>
                    Name  :   {this.props.currentState.name}
                    <br />
                    Username :{this.props.currentState.username}
                    <br />
                    emailId : {this.props.currentState.emailId}
                    <br />
                    mobile :  {this.props.currentState.mobile}
                    <br /><br />
                    <button
                        className="blue"
                        onClick={this.props.openProfile}
                    >
                        Cancel
            </button>
                </div>
            </div>
        );
    }
}



export class Product extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            productDetails: this.props.productDetails
        }
    }

    onIncreament = event => {
        event.preventDefault();
        this.props.updateCartPrice(this.props.productDetails.name,
            this.props.productDetails.price,
            1);
    }

    onDecrement = event => {
        event.preventDefault();
        this.props.updateCartPrice(this.props.productDetails.name,
            -this.props.productDetails.price,
            -1);
    }



    render() {
        return (
            <div>
                <div className="row form-group">
                    <div className="col-sm-2"
                        style={{
                            display: "inline-block"
                        }}
                    >
                        <img
                            src={this.props.productDetails.image_url}
                            alt="new"
                            style={{
                                margin: "0px",
                                width: "200px",
                                height: "100px"
                            }}
                        />
                    </div>
                    <div
                        style={{
                            display: "inline-block",
                            marginLeft: "10px"
                        }}
                    >
                        <h4>{this.props.productDetails.info}</h4>
                    </div>
                    <div className="col-sm-1">
                        <button
                            onClick={this.onIncreament}
                            className="blue"
                            type="button"
                        >
                            +1
                        </button>
                        <button
                            className="blue"
                            type="button"
                            onClick={this.onDecrement}
                        >
                            -1
                        </button>
                    </div>

                    <div className="col-sm-2">
                        <h4> {this.props.productDetails.name} : Rs {this.props.productDetails.price} </h4>
                    </div>
                    <div className="col-sm-2"
                        style={{
                            color: "red"
                        }}
                    >
                        {this.props.productDetails.discount}% OFF

                    </div>
                    <div className="col-sm-2"
                        style={{
                            color: "blue"
                        }}
                    >
                        Retailer : {this.props.productDetails.retailer}
                    </div>
                </div>
                <hr />
            </div>
        );
    }
}







export class MainPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cart: {},
            searchString: "",
            productList: [],
            itemList: [],
            productsFlag: false,
            profileFlag: false,
            cartFlag: false,
            username: "",
            name: "",
            emailId: "",
            mobile: "",
        }
    }


    componentDidMount() {
        this.initialise();
        let token = localStorage.getItem("__Xtoken__");
        GetUserDetails(token).then((result) => {
            if (result.status) {
                this.setState({
                    username: result.responseData.userName,
                    name: result.responseData.userFirstName +
                        " " + result.responseData.userMiddleName +
                        " " + result.responseData.userLastName,
                    mobile: result.responseData.mobile,
                    emailId: result.responseData.emailId,
                });
            } else {
                errorToast("Session Expired,Please Try To Login Again");
            }
        });
    }


    handleChange = (event) => {
        event.preventDefault();
        if (event.target.name === "searchString") {
            GetProductList(event.target.value).then((value) => {
                if (value.status) {
                    this.setState({
                        productList: value.responseData,
                        productsFlag: true,
                    });
                } else {
                    errorToast(value.message);
                }
            });
        }
    }

    handleSearch = () => {
        this.setState({ productsFlag: false }, () => {
            GetProductList(this.state.searchString).then((value) => {
                if (value.status) {
                    this.setState({
                        productList: value.responseData,
                        productsFlag: true,
                    });
                } else {
                    errorToast(value.message);
                }
            });
        });
    }



    makeCartList = () => {
        var itemList = []
        var cart = this.state.cart;
        var keys = Object.keys(cart);
        for (var i = 0; i < keys.length; i++) {
            itemList.push([keys[i], cart[keys[i]]["price"], cart[keys[i]]["quantity"]]);
        }
        this.setState({ itemList: itemList });
    }

    clearCart = () => {
        this.setState({
            cart: null,
            cartFlag: !this.state.cartFlag,
            itemList: [],
        });
    }


    cancel = () => {
        this.setState({ cartFlag: !this.state.cartFlag });
    }


    handleCart = () => {
        var cart = this.state.cart;
        if (cart === null || cart === undefined || cart === {}) {
            errorToast("Cart Is Empty");
        } else {
            this.setState({ cartFlag: true, productList: [] }, () => {
                this.makeCartList();
            });
        }
    }



    calculatePrice = (id, price, quantity) => {
        var cart = this.state.cart;
        if (cart === undefined) {
            cart = {};
            cart[id] = {};
            if (price < 0 && quantity < 0) {
                errorToast("item not present in cart , Invalid Action");
                return;
            }
            cart[id]["price"] = price;
            cart[id]["quantity"] = quantity;
            this.setState({ cart: cart });
        } else if (cart[id]) {
            if ((price < 0 && quantity < 0) && (cart[id]["price"] === 0 && cart[id]["quantity"] === 0)) {
                errorToast("item not present in cart , Invalid Action");
                return;
            }
            cart[id]["price"] += price;
            cart[id]["quantity"] += quantity
            this.setState({ cart: cart });
        } else {
            if (price < 0 && quantity < 0) {
                errorToast("item not present in cart , Invalid Action");
                return;
            }
            cart[id] = {};
            cart[id]["price"] = price;
            cart[id]["quantity"] = quantity;
            this.setState({ cart: cart });
        }
    }


    openProfile = () => {
        this.setState({ profileFlag: !this.state.profileFlag, productList: [] });
    }


    initialise = () => {
        delete this.state.total;
        this.setState({ productList: [] });
        delete this.state.searchString;
        delete this.state.cart;
    }


    mappingfunction = (product) => {
        return (
            <div>
                <Product
                    productDetails={product}
                    updateCartPrice={this.calculatePrice}
                />
            </div>
        );
    }




    render() {
        var products = this.state.productList.map(this.mappingfunction);

        return <div>
            <div >
                <div >
                    <img
                        src={require('../../images/icon.png')}
                        alt=""
                        style={{
                            margin: "0px",
                            width: "200px",
                            height: "90px",
                        }}
                    />
                    <input
                        style={{ marginLeft: 20 }}
                        placeholder="type here to search"
                        name="searchString"
                        type="text"
                        onChange={this.handleChange}
                        required
                    />
                    <button
                        className="btn"
                        type="button"
                        onClick={this.handleSearch}
                    >
                        Search
                    </button>
                    <button
                        className="btn"
                        type="button"
                        onClick={this.openProfile}
                    >
                        Profile
                    </button>
                    <button
                        className="btn"
                        type="button"
                        onClick={this.handleCart}
                    >
                        Cart
                    </button>
                </div>
                <hr />
                {this.state.productsFlag &&
                    <div>
                        {products}
                    </div>
                }

            </div>
            {this.state.profileFlag && <div>
                <Profile currentState={this.state} openProfile={this.openProfile} />
            </div>}
            {this.state.cartFlag && <div>
                <Cart cart={this.state.itemList} clearCart={this.clearCart} cancel={this.cancel} />
            </div>}
            <ToastContainer autoClose={4000} />
        </div>
    }
}

export default MainPage;