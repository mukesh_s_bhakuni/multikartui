import React from 'react';
import { GetUserDetails, CreateUser } from './apiMethods';
import './login.css';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import { successToast, errorToast } from '../utils/utils';
import { Label } from 'reactstrap';
import { Input } from 'semantic-ui-react';


export class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            firstname: "",
            middlename: "",
            lastname: "",
            email: "",
            mobile: "",
            flatno: "",
            society: "",
            street: "",
            landmark: "",
            city: "",
            state: "",
            pincode: "",
            country: "",
            signUpForm: false,
        }
    }


    componentDidMount() {
        this.initialise();
    }





    handleChange = (event) => {
        const name = event.target.name
        this.setState({ [name]: event.target.value })
    }



    handleSignIn = () => {
        if (!this.validateSignIn()) {
            this.errorToast(" username or password not found ")
        } else {
            GetUserDetails(this.state.username, this.state.password).then((value) => {
                if (value.status) {
                    this.setState({ tokenId: value.responseData._id }, () => {
                        localStorage.setItem("__Xtoken__", value.responseData._id);
                        this.props.history.push("newmainpage");
                        successToast("Logged In Successfully");
                    });
                } else {
                    errorToast(value.message);
                }
            });
        }
    }

    handleSignUp = () => {
        var x = this.validateSignUp()
        if (x !== "") {
            errorToast(x);
        } else {
            var body = JSON.stringify({
                userFirstName: this.state.firstname,
                userMiddleName: this.state.middlename,
                userLastName: this.state.lastname,
                emailId: this.state.email,
                userName: this.state.username,
                password: this.state.password,
                mobile: [this.state.mobile],
                user_address: [{
                    flat_no: this.state.flatno,
                    society: this.state.society,
                    street: this.state.street,
                    landmark: this.state.landmark,
                    city: this.state.city,
                    state: this.state.state,
                    pin_code: this.state.pincode,
                    country: this.state.country,
                }]
            });

            CreateUser(body).then((value) => {
                if (value.status) {
                    this.setState({ signUpForm: false }, () => {
                        this.props.history.push("login");
                        successToast("User Created Successfully");
                    });
                } else {
                    errorToast("Unable To Create User");
                }
            });
        }
    }

    signUpForm = () => {
        this.setState({ signUpForm: true });
    }

    signInForm = () => {
        this.setState({ signUpForm: false });
    }

    initialise = () => {
        delete this.state.username;
        delete this.state.firstname;
        // delete this.state.middlename;
        delete this.state.lastname;
        delete this.state.password;
        delete this.state.email;
        delete this.state.mobile;
    }

    validateSignUp = () => {
        let msg = "";
        var username = (this.state.username === null || this.state.username === "" || this.state.username === undefined);
        var password = (this.state.password === null || this.state.password === "" || this.state.password === undefined);
        var firstname = (this.state.firstname === null || this.state.firstname === "" || this.state.firstname === undefined);
        // var middlename = (this.state.middlename === null || this.state.middlename === "" || this.state.middlename === undefined);
        var lastname = (this.state.lastname === null || this.state.lastname === "" || this.state.lastname === undefined);
        var email = (this.state.email === null || this.state.email === "" || this.state.email === undefined);
        var mobile = (this.state.mobile === null || this.state.mobile === "" || this.state.mobile === undefined);
        var flatno = (this.state.flatno === null || this.state.flatno === "" || this.state.flatno === undefined);
        var society = (this.state.society === null || this.state.society === "" || this.state.society === undefined);
        var street = (this.state.street === null || this.state.street === "" || this.state.street === undefined);
        var landmark = (this.state.landmark === null || this.state.landmark === "" || this.state.landmark === undefined);
        var city = (this.state.city === null || this.state.city === "" || this.state.city === undefined);
        var state = (this.state.state === null || this.state.state === "" || this.state.state === undefined);
        var pincode = (this.state.pincode === null || this.state.pincode === "" || this.state.pincode === undefined);
        var country = (this.state.country === null || this.state.country === "" || this.state.country === undefined);

        if (username || password) {
            msg += "[username]"
        }
        if (firstname || lastname) {
            msg += "[name]"
        }
        if (flatno) {
            msg += "[flatno]"
        }
        if (society) {
            msg += "[society]"
        }
        if (street) {
            msg += "[street]"
        }
        if (landmark) {
            msg += "[landmark]"
        }
        if (city) {
            msg += "[city]"
        }
        if (state) {
            msg += "[state]"
        }
        if (pincode) {
            msg += "[pincode]"
        }
        if (country) {
            msg += "[country]"
        }
        if (email) {
            msg += "[email]"
        }
        if (mobile) {
            msg += "[mobile]"
        }
        if (msg === "") {
            return msg;
        } else {
            msg += " Required "
            return msg;
        }
    }

    validateSignIn = () => {
        var username = (this.state.username === null || this.state.username === "" || this.state.username === undefined);
        var password = (this.state.password === null || this.state.password === "" || this.state.password === undefined);
        if (username || password) {
            return 0;
        } else {
            return 1;
        }
    }



    render() {


        return <div className="container"
            style={{
                overflow: "scroll",
                background: "gray"
            }}
        >
            {!this.state.signUpForm && <div className="box"
                style={{
                    maxWidth: "400px"
                }}
            >

                <div>
                    <div className="row field"
                        style={{
                            textAlign: "left",
                            color: "white"
                        }}
                    >
                        Username
                   <input
                            style={{ marginLeft: 1 }}
                            placeholder="Username/email"
                            name="username"
                            type="text"
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div className="row field"
                        style={{
                            textAlign: "left",
                            color: "white"
                        }}
                    >
                        Password
                   <input
                            style={{ marginLeft: 1 }}
                            placeholder="Password"
                            name="password"
                            type="password"
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                </div>
                <br /><br />
                <div className="row"
                    style={{ paddingLeft: 20 }}>
                    <button
                        className="lgreen"
                        type="button"
                        onClick={this.handleSignIn}
                    >
                        SignIn
                </button>
                    <button
                        className="cyan"
                        type="button"
                        onClick={this.signUpForm}
                    >
                        SignUp
                </button>
                </div>
                <div className="row">
                    <button
                        className="blue"
                        type="button btn-link"
                        onClick={this.signUpForm}
                    >
                        forgot password
                </button>
                </div>
            </div>}

            {this.state.signUpForm && <div className="box">
                <div>
                    <div
                        style={{ textAlign: "center", color: "white" }}
                    >
                        <b><u>Basic Details</u></b>
                    </div>
                    <br />
                    <div
                        style={{
                            textAlign: "left",
                            display: "flex",
                            justifyContent: "space-between"
                        }}
                    >
                        <div>
                            <Label
                                style={{
                                    color: "white"
                                }}
                            >First Name</Label>
                            <Input
                                style={{ width: "100%" }}
                                name="firstname"
                                value={this.state.firstName}
                                placeholder="First Name"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div>
                            <Label
                                style={{
                                    color: "white"
                                }}
                            >Middle Name</Label>
                            <Input
                                style={{ width: "100%" }}
                                name="middlename"
                                value={this.state.middlename}
                                placeholder="Middle Name"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div>
                            <Label
                                style={{
                                    color: "white"
                                }}
                            >Last Name</Label>
                            <Input
                                style={{ width: "100%" }}
                                name="lastname"
                                value={this.state.lastname}
                                placeholder="Last Name"
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div
                        style={{
                            textAlign: "left",
                            display: "flex",
                            justifyContent: "space-between"
                        }}
                    >
                        <div>
                            <Label
                                style={{
                                    color: "white"
                                }}
                            >Email</Label>
                            <Input
                                style={{ width: "100%" }}
                                name="email"
                                value={this.state.email}
                                placeholder="Email"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div>
                            <Label
                                style={{
                                    color: "white"
                                }}
                            >Username</Label>
                            <Input
                                style={{ width: "100%" }}
                                name="username"
                                value={this.state.username}
                                placeholder="Username"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div>
                            <Label
                                style={{
                                    color: "white"
                                }}
                            >Password</Label>
                            <Input
                                style={{ width: "100%" }}
                                name="password"
                                value={this.state.password}
                                placeholder="Password"
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div
                        style={{
                            textAlign: "left"
                        }}
                    >
                        <Label
                            style={{
                                color: "white"
                            }}
                        >Mobile</Label>
                        <Input
                            style={{ width: "24%" }}
                            name="mobile"
                            value={this.state.mobile}
                            placeholder="Mobile"
                            onChange={this.handleChange}
                        />
                    </div>
                </div>
                <div>
                    <Label
                        style={{
                            color: "white",
                            fontWeight: "bold",
                            textDecoration: "underline"
                        }}
                    >Address</Label>
                </div>
                <br /><br />
                <div
                    style={{
                        textAlign: "left",
                        display: "flex",
                        justifyContent: "space-between"
                    }}
                >
                    <div>
                        <Label
                            style={{
                                color: "white"
                            }}
                        >Flat No</Label>
                        <Input
                            style={{ width: "100%" }}
                            name="flatno"
                            value={this.state.flatno}
                            placeholder="Flat No"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div>
                        <Label
                            style={{
                                color: "white"
                            }}
                        >Society</Label>
                        <Input
                            style={{ width: "100%" }}
                            name="society"
                            value={this.state.society}
                            placeholder="Society"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div>
                        <Label
                            style={{
                                color: "white"
                            }}
                        >Street</Label>
                        <Input
                            style={{ width: "100%" }}
                            name="street"
                            value={this.state.street}
                            placeholder="Street"
                            onChange={this.handleChange}
                        />
                    </div>
                </div>
                <div
                    style={{
                        textAlign: "left",
                        display: "flex",
                        justifyContent: "space-between"
                    }}
                >
                    <div>
                        <Label
                            style={{
                                color: "white"
                            }}
                        >Landmark</Label>
                        <Input
                            style={{ width: "100%" }}
                            name="landmark"
                            value={this.state.landmark}
                            placeholder="Landmark"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div>
                        <Label
                            style={{
                                color: "white"
                            }}
                        >City</Label>
                        <Input
                            style={{ width: "100%" }}
                            name="City"
                            value={this.state.city}
                            placeholder="City"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div>
                        <Label
                            style={{
                                color: "white"
                            }}
                        >State</Label>
                        <Input
                            style={{ width: "100%" }}
                            name="state"
                            value={this.state.state}
                            placeholder="State"
                            onChange={this.handleChange}
                        />
                    </div>
                </div>
                <div
                    style={{
                        textAlign: "left",
                        display: "flex",
                        justifyContent: "flex-start"
                    }}
                >
                    <div>
                        <Label
                            style={{
                                color: "white"
                            }}
                        >Pincode</Label>
                        <Input
                            style={{ width: "100%" }}
                            name="pincode"
                            value={this.state.pincode}
                            placeholder="Pincode"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div
                        style={{
                            marginLeft: "142px"
                        }}
                    >
                        <Label
                            style={{
                                color: "white"
                            }}
                        >Country</Label>
                        <Input
                            style={{ width: "100%" }}
                            name="country"
                            value={this.state.country}
                            placeholder="Country"
                            onChange={this.handleChange}
                        />
                    </div>

                </div>
                <br /><br /><br />
                <div
                    style={{
                        display: "flex",
                        justifyContent: "center"
                    }}
                >
                    <div>
                        <button
                            className="lgreen"
                            type="button"
                            onClick={this.handleSignUp}
                        >
                            Submit
                        </button>
                    </div>
                    <div>
                        <button
                            className="red"
                            type="button"
                            onClick={this.signInForm}
                        >
                            Cancel
                        </button>
                    </div>
                </div>

                {/* <div>
                    <div className="row"
                        style={{
                            textAlign: "center"
                        }}
                    >
                        <b><u>Basic Details</u></b>
                    </div>
                    <div className="row"
                        style={{
                            textAlign: "left"
                        }}
                    >
                        <Label>First Name</Label>
                   <input
                            style={{ marginLeft: 1, width: "40%" }}
                            placeholder="first name"
                            name="firstname"
                            type="text"
                            onChange={this.handleChange}
                            required
                    />
                    </div>
                    <div className="row">
                        Middle Name
                   <input
                            style={{ marginLeft: 1 }}
                            placeholder="middle name"
                            name="middlename"
                            type="text"
                            onChange={this.handleChange}
                            required
                   />
                    </div>
                    <div className="row inputField">
                        Last Name
                   <input
                            style={{ marginLeft: 1 }}
                            placeholder="last name"
                            name="lastname"
                            type="text"
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div className="row inputField">
                        Email
                   <input
                            style={{ marginLeft: 1 }}
                            placeholder="em@il"
                            name="email"
                            type="text"
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div className="row inputField">
                        Username
                   <input
                            style={{ marginLeft: 1 }}
                            placeholder="username"
                            name="username"
                            type="text"
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div className="row inputField">
                        Password
                   <input
                            style={{ marginLeft: 1 }}
                            placeholder="password"
                            name="password"
                            type="password"
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div className="row inputField">
                        Mobile
                   <input
                            style={{ marginLeft: 1 }}
                            placeholder="mobile"
                            name="mobile"
                            type="text"
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <br /><br />
                    <div className="row inputField"
                        style={{
                            textAlign: "center"
                        }}
                    >
                        <b><u>Address</u></b>
                    </div>
                    <br /><br />
                    <div className="row inputField">
                        <input
                            style={{ marginLeft: "0px" }}
                            placeholder="Flat No"
                            name="flatno"
                            type="text"
                            onChange={this.handleChange}
                            required
                        />
                        <input
                            style={{ marginLeft: "1px" }}
                            placeholder="Society"
                            name="society"
                            type="text"
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div className="row inputField">
                        <input
                            style={{ marginLeft: "0px" }}
                            placeholder="Street"
                            name="street"
                            type="text"
                            onChange={this.handleChange}
                            required
                        />
                        <input
                            style={{ marginLeft: "1px" }}
                            placeholder="Landmark"
                            name="landmark"
                            type="text"
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div className="row inputField">
                        <input
                            style={{ marginLeft: "0px" }}
                            placeholder="City"
                            name="city"
                            type="text"
                            onChange={this.handleChange}
                            required
                        />
                        <input
                            style={{ marginLeft: "1px" }}
                            placeholder="State"
                            name="state"
                            type="text"
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div className="row inputField">
                        <input
                            style={{ marginLeft: "0px" }}
                            placeholder="Pin Code"
                            name="pincode"
                            type="text"
                            onChange={this.handleChange}
                            required
                        />
                        <input
                            style={{ marginLeft: "1px" }}
                            placeholder="Country"
                            name="country"
                            type="text"
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                </div>
                <br /><br />
                <div className="row"
                    style={{ paddingLeft: 20 }}>
                    <button
                        className="lgreen"
                        type="button"
                        onClick={this.handleSignUp}
                    >
                        Submit
                </button>
                    <button
                        className="red"
                        type="button"
                        onClick={this.signInForm}
                    >
                        Cancel
                </button>
                </div> */}
            </div>}
            <ToastContainer autoClose={4000} />
        </div>
    }
}

export default Login;