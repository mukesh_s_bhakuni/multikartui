import { DOMAIN } from '../../constants/http.js'

export function GetUserDetails(username, password) {
    return fetch(DOMAIN + `multiKart/login/?changeType=login`, {
        method: 'PUT',
        body: JSON.stringify({
            username: username,
            password: password
        }),
    })
        .then(res => res.json())
        .then((result) => {
            return result;
        },
            (error) => {
                console.log(error)
                return error
            }
        ).catch((error) => { console.log(error) });
}


export function CreateUser(body) {
    return fetch(DOMAIN + `multiKart/user/`, {
        method: 'POST',
        body: body,
    })
        .then(res => res.json())
        .then((result) => {
            return result
        },
            (error) => {
                console.log(error)
                return error
            }
        ).catch((error) => { console.log(error) });


}