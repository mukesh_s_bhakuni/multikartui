export function GetUserCart(token) {
    return fetch(`http://localhost:7707/multiKart/cart/?token=${token}`, {
        method: 'GET',
    })
        .then(res => res.json())
        .then((result) => {
            return result
        },
            (error) => {
                console.log(error)
                return error
            }
        ).catch((error) => { console.log(error) });
}

export function RemoveProductFromCart(body) {
    console.log(body);
    return fetch(`http://localhost:7707/multiKart/cart/?option=updateCart`, {
        method: 'PUT',
        body : body
    })
        .then(res => res.json())
        .then((result) => {
            return result
        },
            (error) => {
                console.log(error)
                return error
            }
        ).catch((error) => { console.log(error) });
}
