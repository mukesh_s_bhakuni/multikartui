import React from 'react';

import './cart.css';
import { GetUserCart,RemoveProductFromCart } from './apiMethods';
import { errorToast, successToast } from '../utils/utils';
import { Label } from 'semantic-ui-react';


class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            cartId : "",
            status: "",
            tax: "",
            total_price: 0,
            shipping_price: 0,
            estimated_total: 0,
            user_id : ""
        }
    }

    componentDidMount() {
        var token = localStorage.getItem("__Xtoken__");
        GetUserCart(token).then(result => {
            if (result.status) {
                this.setState({
                    cartId : result.responseData._id,
                    products: result.responseData.products,
                    status: result.responseData.status,
                    tax: result.responseData.tax,
                    total_price: result.responseData.total_price,
                    shipping_price: result.responseData.shipping_price,
                    estimated_total: result.responseData.estimated_total,
                    user_id : result.responseData.user_id
                });
            } else {
                errorToast("no items in cart, cart empty");
            }
        });
    }

    

    removeProduct = (product_id) => {
       var products = this.state.products; 
       return products.filter(product => product.product._id !== product_id);
    }

    removeFromCart = (id) => {
        var products = this.removeProduct(id);
        var body = JSON.stringify({
            "_id" : this.state.cartId,
            "products" : products,
            "status" : "Pending",
            "user_id" : this.state.user_id
        });
        RemoveProductFromCart(body).then(result => {
            if (result.status) {
                successToast("item removed");
                this.componentDidMount();
            } else {
                errorToast("unable to remove item from cart");
            }
        });
    }

    mappingFunction = (product) => {
        return (
            <div className="cart-card">
                <img
                    src={product.product.image_url}
                    alt="new"
                    style={{
                        margin: "0px",
                        width: "250px",
                        height: "250px"
                    }}
                />
                <br />
                <Label><b>{product.product.name}</b></Label>
                <br />
                <Label><b>Rs {product.product.price}</b></Label>
                <br />
                <Label>Retailer : {product.product.retailer}</Label>
                <br />
                <button
                    style={{
                        marginLeft: "2px",
                        background: "#111",
                        color: "white",
                        width: "150px"
                    }}
                    onClick={this.removeFromCart.bind(this, product.product._id)}
                >
                    Remove
                </button>
                <br /><br />
            </div>
        );
    }

    checkOut = () => {
        console.log("not yet implemented");
    }

    render() {
        var products = this.state.products;
        if (products !== [] && products !== undefined && products !== null) {
            products = products.map(this.mappingFunction);
        }    
        return <div
            style={{
                overflowY: "scroll"
            }}
        >
            <div className="cart">
                <div
                    style={{
                        color: "white",
                        fontWeight: "bold",
                        display: "flex",
                        justifyContent: "center"
                    }}
                >
                    <Label>Cart</Label>
                </div>
                { products !== [] && products !== undefined && products !== null &&
                <div
                    style={{
                        marginTop: "20px",
                        display: "flex",
                        justifyContent: "space-between",
                        overflowX: "scroll",
                        overflowY: "hidden"
                    }}
                >
                
                    {products}
                
                </div>
                }
                { products !== [] && products !== undefined && products !== null &&
                <div
                  style={{
                     color : "white",
                     display : "flex",
                     justifyContent : "space-between"      
                  }}  
                >
                    <Label><b>Total Price : {this.state.total_price}</b></Label>
                    <br />
                    <Label><b>Shipping Price : {this.state.shipping_price}</b></Label>
                    <br />
                    <Label><b>Tax  : {this.state.tax}</b></Label>
                    <br />
                    <Label><b>Estimated Total : {this.state.estimated_total}</b></Label>
                </div>
                }
                <div
                    style={{
                        marginTop: "18px",
                        display: "flex",
                        justifyContent: "center"
                    }}
                >
                   {products !== [] && products !== undefined && products !== null &&
                    <button
                        style={{
                            background: "green",
                            color: "white"
                        }}
                        onClick={this.checkOut}
                    >
                        CheckOut
                    </button>
                   }
                    <button
                        style={{
                            background: "red",
                            color: "white"
                        }}
                        onClick={this.props.callBackFunc.bind(this, "cancel")}
                    >
                        Cancel
                    </button>
                </div>
            </div>

        </div>
    }
}

export default Cart;


