// import DOMAIN from '../../constants/http';

export function AddToCart(token,productId,quantity) {
    return fetch(`http://localhost:7707/multiKart/cart/?option=addProduct&userToken=${token}&productId=${productId}&quantity=${quantity}`, {
        method: 'PUT',
    })
        .then(res => res.json())
        .then((result) => {
            return result
        },
            (error) => {
                console.log(error)
                return error
            }
        ).catch((error) => { console.log(error) });
}