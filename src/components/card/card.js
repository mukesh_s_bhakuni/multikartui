import React from 'react';
import './card.css';
import {Label} from 'reactstrap';
import {errorToast, successToast} from '../utils/utils';
import {AddToCart} from './apiMethods';



export class Product extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loader : false,
            productDetails: this.props.productDetails
        }
    }

    onIncreament = event => {
        event.preventDefault(); 
        this.props.updateCartPrice(this.props.productDetails.name,
            this.props.productDetails.price,
            1);
    }

    onDecrement = event => {
        event.preventDefault();
        this.props.updateCartPrice(this.props.productDetails.name,
            -this.props.productDetails.price,
            -1);
    }


    addToCart = () =>{
        this.setState({loader : true})
        var token = localStorage.getItem("__Xtoken__");
        AddToCart(token,this.props.productDetails._id,"1").then(result => {
            if (result.status) {
                successToast("item added to cart");
                this.setState({loader : false});
            } else {
                errorToast("unable to add item to cart");
                this.setState({loader : false});
            }
        });
    }



    render() {
        return (
            <div className="card-container">
            {
                this.state.loader &&
                 <div>
                     <img src="../../images/loader.gif" 
                      alt="new"
                      style={{
                            margin: "0px",
                            width: "250px",
                            height: "250px"
                      }}
                     />
                 </div>    
            }
            {!this.state.loader &&
            <div>
                <img 
                    src={this.props.productDetails.image_url}
                    alt="new"
                    style={{
                        margin: "0px",
                        width: "250px",
                        height: "250px"
                    }}
                />
                <br />
                <Label><b>{this.props.productDetails.name}</b></Label>
                <br />
                <Label><b>Rs {this.props.productDetails.price}</b></Label>    
                <br />
                <Label>{this.props.productDetails.info}</Label>
                <br />
                <Label>Retailer : </Label>
                <Label>{this.props.productDetails.retailer}</Label> 
                <br />
                <Label>{this.props.productDetails.discount}% OFF</Label>
                <br /><br />
                <button
                style={{
                    marginLeft : "2px",
                    background : "#111",
                    color : "white",
                    width : "150px"
                }}
                onClick={this.addToCart}
                >
                Add to Cart
                </button>    
            </div>
            }
            </div>
        );
    }
}




class Card extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            productList : this.props.productList,
            cart : {},
            itemList : [],
        }
    }

    

    calculatePrice = (id, price, quantity) => {
        var cart = this.state.cart;
        if (cart === undefined) {
            cart = {};
            cart[id] = {};
            if (price < 0 && quantity < 0) {
                errorToast("item not present in cart , Invalid Action");
                return;
            }
            cart[id]["price"] = price;
            cart[id]["quantity"] = quantity;
            this.setState({ cart: cart });
        } else if (cart[id]) {
            if ((price < 0 && quantity < 0) && (cart[id]["price"] === 0 && cart[id]["quantity"] === 0)) {
                errorToast("item not present in cart , Invalid Action");
                return;
            }
            cart[id]["price"] += price;
            cart[id]["quantity"] += quantity;
            this.setState({ cart: cart });
        } else {
            if (price < 0 && quantity < 0) {
                errorToast("item not present in cart , Invalid Action");
                return;
            }
            cart[id] = {};
            cart[id]["price"] = price;
            cart[id]["quantity"] = quantity;
            this.setState({ cart: cart });
        }
        this.props.callBackFunc(this.state.cart);
    }



   


    mappingFunction = (product) => {
        return (
            <div className="card">
                <Product
                    productDetails={product}
                    updateCartPrice={this.calculatePrice}
                />
            </div>
        );
    }


    render() {
        var products = this.props.productList.map(this.mappingFunction);
        return <div className="band"
           style={{
                display : "flex",
                justifyContent : "space-between"               
           }}
        >
            {products}
        </div>
    }
}


export default Card;


